//
//  LoginCoordinator.swift
//  Test
//
//  Created by Filip Laššo on 15/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import Foundation
import UIKit

final class LoginCoordinator: Coordinator {
    
    // MARK: - Properties

    private let navigationController: UINavigationController

    // MARK: - Lifecycle

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    override func start() {
        let controller = LoginViewController.instantiateViewController()
        controller.onLoginSelected = { [weak self] in
            self?.presentedController?.dismiss(animated: true, completion: {
                self?.finish()
            })
        }
        navigationController.setViewControllers([controller], animated: false)
        navigationController.modalPresentationStyle = .overFullScreen
        presentedController?.present(navigationController, animated: true, completion: nil)
    }
    
}
