//
//  FirstCoordinator.swift
//  Test
//
//  Created by Filip Laššo on 11/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import Foundation
import UIKit

final class FirstCoordinator: Coordinator {

    // MARK: - Properties

    private let navigationController: UINavigationController

    // MARK: - Lifecycle

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    override func start() {
        let controller = ViewController1.instantiateViewController()
        controller.onAmiiboSelected = { [weak self] in
            self?.pushAmiibos(callback: { [weak controller] name in controller?.amiibo = name })
        }
        controller.onLoginSelected = { [weak self] in
            let navigationController = UINavigationController()
            let loginCoordinator = LoginCoordinator(navigationController: navigationController)
            self?.startChildCoordinator(loginCoordinator)
        }
        navigationController.setViewControllers([controller], animated: false)
    }
    
    private func pushAmiibos(callback: @escaping (String) -> Void) {
        let controller = ViewController1a.instantiateViewController()
        controller.onAmiiboSelected = { [weak self] name in
            callback(name)
            self?.navigationController.popViewController(animated: true)
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
}
