//
//  ThirdCoordinator.swift
//  Test
//
//  Created by Filip Laššo on 11/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import Foundation
import UIKit

final class ThirdCoordinator: Coordinator {

    // MARK: - Properties

    private let navigationController: UINavigationController

    // MARK: - Lifecycle

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    override func start() {
        let controller = ViewController3.instantiateViewController()
        controller.onAmiiboSelected = { [weak self] imageUrl in
            self?.modal(imageUrl: imageUrl)
        }
        navigationController.setViewControllers([controller], animated: false)
    }
    
    private func modal(imageUrl: String) {
        let controller = ModalViewController.instantiateViewController()
        controller.amiiboImageUrl = imageUrl
        controller.onModalClosed = { [weak controller] in
            controller?.dismiss(animated: true, completion: nil)
        }
        presentedController?.present(controller, animated: true, completion: nil)
    }
    
}
