//
//  AppCoordinator.swift
//  Test
//
//  Created by Filip Laššo on 11/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import Foundation
import UIKit

final class AppCoordinator: Coordinator {
    
    // MARK: - Properties
    
    private let window: UIWindow

    // MARK: - Lifecycle
    
    init(in window: UIWindow) {
        self.window = window
    }
    
    override func start() {
        let tabBarController = TabBarController()

        let firstNavigationController = UINavigationController()
        firstNavigationController.tabBarItem = UITabBarItem(title: "1", image: nil, tag: 0)
        let firstCoordinator = FirstCoordinator(navigationController: firstNavigationController)

        let secondNavigationController = UINavigationController()
        secondNavigationController.tabBarItem = UITabBarItem(title: "2", image: nil, tag: 1)
        let secondCoordinator = SecondCoordinator(navigationController: secondNavigationController)

        let thirdNavigationController = UINavigationController()
        thirdNavigationController.tabBarItem = UITabBarItem(title: "3", image: nil, tag: 2)
        let thirdCoordinator = ThirdCoordinator(navigationController: thirdNavigationController)

        tabBarController.viewControllers = [firstNavigationController,
                                            secondNavigationController,
                                            thirdNavigationController]

        window.rootViewController = tabBarController
        window.makeKeyAndVisible()

        startChildCoordinator(firstCoordinator)
        startChildCoordinator(secondCoordinator)
        startChildCoordinator(thirdCoordinator)
    }
    
}
