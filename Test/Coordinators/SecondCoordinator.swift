//
//  SecondCoordinator.swift
//  Test
//
//  Created by Filip Laššo on 11/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import Foundation
import UIKit

final class SecondCoordinator: Coordinator {

    // MARK: - Properties

    private let navigationController: UINavigationController

    // MARK: - Lifecycle

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    override func start() {
        let controller = ViewController2.instantiateViewController()
        controller.onNextSelected = { [weak self] name in
            self?.push2a(name: name)
        }
        navigationController.setViewControllers([controller], animated: false)
    }
    
    private func push2a(name: String?) {
        let controller = ViewController2a.instantiateViewController()
        if let unwrappedName = name {
            controller.name = unwrappedName
        }
        controller.onNextSelected = { [weak self] in
            self?.push2b()
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func push2b() {
        let controller = ViewController2b.instantiateViewController()
        navigationController.pushViewController(controller, animated: true)
    }
    
}
