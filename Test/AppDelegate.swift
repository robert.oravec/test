//
//  AppDelegate.swift
//  Test
//
//  Created by Filip Laššo on 14/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit
#if DEBUG
import netfox
#endif

@UIApplicationMain
class AppDelegate: UIResponder,
    UIApplicationDelegate {
    
    // MARK: - Properties
    
    var window: UIWindow?
    private var appCoordinator: AppCoordinator!
    
    // MARK: - Lifecycle
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        #if DEBUG
        NFX.sharedInstance().start()
        #endif
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        appCoordinator = AppCoordinator(in: window!)
        appCoordinator.start()
        
        return true
    }
    
}
