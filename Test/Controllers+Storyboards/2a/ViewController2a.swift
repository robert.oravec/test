//
//  ViewController2a.swift
//  Test
//
//  Created by Filip Laššo on 14/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit

final class ViewController2a: UIViewController,
    StoryboardInstantiable {
    
    // MARK: - Properties
    
    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var nextButton: UIButton!
    
    var name: String = "Robha"
    
    var onNextSelected: (() -> Void)?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoLabel.text = String(format: NSLocalizedString("Go deeper", comment: ""), name)
        nextButton.setTitle(NSLocalizedString("Next", comment: ""), for: .normal)
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    // MARK: - Action
    
    @IBAction private func nextButtonDidTap(_ sender: Any) {
        onNextSelected?()
    }
    
}
