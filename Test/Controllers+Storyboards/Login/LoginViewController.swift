//
//  LoginViewController.swift
//  Test
//
//  Created by Filip Laššo on 15/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import UIKit
import KeychainSwift

final class LoginViewController: UIViewController,
    StoryboardInstantiable {

    // MARK: - Properties

    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var loginButton: UIButton!
    
    var onLoginSelected: (() -> Void)?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    // MARK: - Action
    
    @IBAction private func loginButtonDidTap(_ sender: Any) {
        // validation
        if !isValidEmail() {
            emailTextField.text = ""
            emailTextField.placeholder = NSLocalizedString("Invalid email", comment: "")
            return
        }
        
        if !isValidPassword() {
            passwordTextField.text = ""
            passwordTextField.placeholder = NSLocalizedString("Invalid password", comment: "")
            return
        }
        
        // save pass to keychain
        let keychain = KeychainSwift()
        keychain.set(passwordTextField.text!, forKey: KeychainSwift.Keys.password)
        
        // set logged user to user defaults
        UserDefaults.standard.set(emailTextField.text!, forKey: UserDefaults.Keys.loggedUser)
        
        loginButton.setTitle(NSLocalizedString("Logout", comment: ""), for: .normal)
        
        onLoginSelected?()
    }
    
    // MARK: - UI
    
    private func setupUI() {
        title = NSLocalizedString("Login", comment: "")
        emailTextField.placeholder = NSLocalizedString("Email", comment: "")
        passwordTextField.placeholder = NSLocalizedString("Password", comment: "")
        loginButton.setTitle(NSLocalizedString("Login", comment: ""), for: .normal)
    }
    
    // MARK: - Input validation
    
    private func isValidEmail() -> Bool {
        guard let email = emailTextField.text else { return false }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    private func isValidPassword() -> Bool {
        guard let password = passwordTextField.text else { return false }
        
        return password.count >= 6
    }
    
}
