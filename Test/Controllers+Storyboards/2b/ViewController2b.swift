//
//  ViewController2b.swift
//  Test
//
//  Created by Filip Laššo on 20/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit

final class ViewController2b: UIViewController,
    StoryboardInstantiable {
    
    // MARK: - Properties
    
    @IBOutlet private weak var infoLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoLabel.text = NSLocalizedString("The deepest shit!", comment: "")
    }
    
    deinit {
        print("deinit \(self)")
    }
    
}
