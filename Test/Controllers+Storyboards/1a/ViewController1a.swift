//
//  ViewController1a.swift
//  Test
//
//  Created by Filip Laššo on 14/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit

final class ViewController1a: UIViewController,
    StoryboardInstantiable,
    UITableViewDelegate,
    UITableViewDataSource {
    
    // MARK: - Properties
    
    @IBOutlet private var tableView: UITableView!
    private var amiiboNames = [String]()
    
    var onAmiiboSelected: ((String) -> Void)?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        let urlSession = URLSession(configuration: .default)
        let networkingManager = NetworkingManager(urlSession: urlSession)
        
        networkingManager.request(to: AmiiboRouter.getAll) { [weak self] (result: Result<AmiiboResponse, Error>) in
            guard let self = self else { return }
            switch result {
            case .success(let amiiboResponse):
                self.amiiboNames = amiiboResponse.amiibo.map { $0.name }
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        onAmiiboSelected?(amiiboNames[indexPath.row])
    }

    // MARK: - UITableViewDataSource

    func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return amiiboNames.count
    }
    
    func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = amiiboNames[indexPath.row]
        return cell
    }
    
}
