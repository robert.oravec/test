//
//  ViewController1.swift
//  Test
//
//  Created by Filip Laššo on 14/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit
import KeychainSwift

final class ViewController1: UIViewController,
    StoryboardInstantiable {
    
    // MARK: - Properties
    
    @IBOutlet private weak var selectAmiiboButton: UIButton!
    var amiibo: String? {
        didSet {
            selectAmiiboButton.setTitle(amiibo, for: .normal)
        }
    }
    
    @IBOutlet private weak var loginButton: UIButton!
    
    var onAmiiboSelected: (() -> Void)?
    var onLoginSelected: (() -> Void)?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let keychain = KeychainSwift()
        print(keychain.get(KeychainSwift.Keys.password))
        print(UserDefaults.standard.string(forKey: UserDefaults.Keys.loggedUser))
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    // MARK: - Action
    
    @IBAction private func selectAmiiboButtonDidTap(_ sender: Any) {
        onAmiiboSelected?()
    }
    
    @IBAction private func loginButtonDidTap(_ sender: Any) {
        if loginButton.title(for: .normal) == NSLocalizedString("Login", comment: "")  {
            login()
        } else {
            logout()
        }
    }
    
    // MARK: - UI
    
    private func setupUI() {
        selectAmiiboButton.setTitle(NSLocalizedString("Amiibo", comment: ""), for: .normal)
        loginButton.setTitle(NSLocalizedString(UserDefaults.standard.string(forKey: UserDefaults.Keys.loggedUser) == nil ? "Login" : "Logout", comment: ""), for: .normal)
    }
    
    // MARK: - Flow
    
    private func login() {
        loginButton.setTitle(NSLocalizedString("Logout", comment: ""), for: .normal)
        onLoginSelected?()
    }
    
    private func logout() {
        // remove logged user from user defaults
        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.loggedUser)
        
        loginButton.setTitle(NSLocalizedString("Login", comment: ""), for: .normal)
    }
    
}
