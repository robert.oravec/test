//
//  ModalViewController.swift
//  Test
//
//  Created by Filip Laššo on 20/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit

final class ModalViewController: UIViewController,
    StoryboardInstantiable {
    
    // MARK: - Properties
    
    @IBOutlet private weak var amiiboImageView: UIImageView!
    var amiiboImageUrl: String!
    
    var onModalClosed: (() -> Void)?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        amiiboImageView.loadImageAsync(with: amiiboImageUrl)
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    // MARK: - Action
    
    @IBAction private func closeButtonDidTap(_ sender: Any) {
        onModalClosed?()
    }
    
}
