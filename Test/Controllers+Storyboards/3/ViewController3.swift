//
//  ViewController3.swift
//  Test
//
//  Created by Filip Laššo on 14/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit

final class ViewController3: UIViewController,
    StoryboardInstantiable,
    UITableViewDelegate,
    UITableViewDataSource {
    
    // MARK: - Properties
    
    private var amiibos = [Amiibo]()
    @IBOutlet private weak var tableView: UITableView!
    
    var onAmiiboSelected: ((String) -> Void)?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        let urlSession = URLSession(configuration: .default)
        let networkingManager = NetworkingManager(urlSession: urlSession)
        
        networkingManager.request(to: AmiiboRouter.getAll) { [weak self] (result: Result<AmiiboResponse, Error>) in
            guard let self = self else { return }
            switch result {
            case .success(let amiiboResponse):
                self.amiibos = amiiboResponse.amiibo
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        onAmiiboSelected?(amiibos[indexPath.row].image)
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return amiibos.count
    }
    
    func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.amiiboName = amiibos[indexPath.row].name
        cell.amiiboImageUrl = amiibos[indexPath.row].image
        return cell
    }
    
}
