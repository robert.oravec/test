//
//  TableViewCell.swift
//  Test
//
//  Created by Filip Laššo on 20/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit

final class TableViewCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet private weak var amiiboImageView: UIImageView!
    var amiiboImageUrl: String? {
        didSet {
            amiiboImageView.loadImageAsync(with: amiiboImageUrl)
        }
    }
    
    @IBOutlet private weak var amiiboNameLabel: UILabel!
    var amiiboName: String! {
        didSet {
            amiiboNameLabel.text = amiiboName
        }
    }
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
