//
//  ViewController2.swift
//  Test
//
//  Created by Filip Laššo on 14/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit

final class ViewController2: UIViewController,
    StoryboardInstantiable {
    
    // MARK: - Properties
    
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var nextButton: UIButton!
    
    var onNextSelected: ((String?) -> Void)?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.placeholder = NSLocalizedString("Your name", comment: "")
        nextButton.setTitle(NSLocalizedString("Next", comment: ""), for: .normal)
    }
    
    deinit {
        print("deinit \(self)")
    }
    
    // MARK: - Action
    
    @IBAction private func nextButtonDidTap(_ sender: Any) {
        let name = (nameTextField.text ?? "").isEmpty ? nil : nameTextField.text
        onNextSelected?(name)
    }
    
}
