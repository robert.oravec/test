//
//  AmiiboRouter.swift
//  Test
//
//  Created by Filip Laššo on 28/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

public enum AmiiboRouter: Routable {
    
    case getAll
    case test(page: Int, pageSize: Int)
    
    var httpMethod: HTTPMethod {
        switch self {
        case .getAll,
             .test:
            return .get
        }
    }
    
    var path: String {
        return "api/amiibo/"
    }
    
    var queryParameters: [String : String]? {
        switch self {
        case .test(let page, let pageSize):
            return [
                "page" : String(page),
                "pageSize" : String(pageSize)
            ]
        default:
            return nil
        }
    }
    
    var headerParameters: [String : String]? {
        switch self {
        case .test:
            return [
                "Content-Type" : "application/json"
            ]
        default:
            return [
                "Content-Type" : "application/x-www-form-urlencoded"
            ]
        }
    }
    
    var isAuthorizationRequired: Bool {
        switch self {
        case .getAll:
            return true
        default:
            return false
        }
    }
    
    var bodyParameters: [String : Any]? {
        switch self {
        case .test:
            return [
                "password" : "123",
                "user" : "admin"
            ]
        default:
            return nil
        }
    }
    
}
