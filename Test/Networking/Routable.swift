//
//  Routable.swift
//  Test
//
//  Created by Filip Laššo on 22/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit

protocol Routable {
    
    var httpMethod: HTTPMethod { get }
    var path: String { get }
    var queryParameters: [String : String]? { get }
    var headerParameters: [String : String]? { get }
    var isAuthorizationRequired: Bool { get }
    var bodyParameters: [String : Any]? { get }
    
}

extension Routable {
    
    var scheme: HTTPScheme {
        return .https
    }
    
    var host: String {
        return "www.amiiboapi.com"
    }
    
    var url: URL {
        var components = URLComponents()
        components.scheme = scheme.rawValue
        components.host = host
        components.path = "/\(path)"
        if let queryParameters = queryParameters {
            components.queryItems = queryParameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        }
        
        return components.url! // tu nie je s ! co dojebat predpokladam ked nenapisem napicu jednotlive casti URL
    }
    
    var urlRequest: URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        urlRequest.allHTTPHeaderFields = headerParameters
        if isAuthorizationRequired {
            urlRequest.setValue("Bearer \(123)", forHTTPHeaderField: "Authorization")
        }
        if let bodyParameters = bodyParameters {
            HTTPBodyEncoder().encode(urlRequest: &urlRequest, with: bodyParameters)
        }
        
        return urlRequest
    }
    
}
