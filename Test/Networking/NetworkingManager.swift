//
//  NetworkingManager.swift
//  Test
//
//  Created by Filip Laššo on 28/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import Foundation

class NetworkingManager {
    
    let urlSession: URLSession
    
    init(urlSession: URLSession) {
        self.urlSession = urlSession
    }
    
    func request<T: Codable>(
        to router: Routable,
        completion: @escaping (Result<T, Error>) -> ()
    ) {
        urlSession.dataTask(with: router.urlRequest) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard response != nil, let data = data else { return }

            DispatchQueue.main.async {
                do {
                    let responseObject = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(responseObject))
                } catch let error {
                    print(error)
                }
            }
        }.resume()
    }
    
}
