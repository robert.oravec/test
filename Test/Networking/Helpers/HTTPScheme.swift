//
//  HTTPScheme.swift
//  Test
//
//  Created by Filip Laššo on 29/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

public enum HTTPScheme: String {
    
    case http = "http"
    case https = "https"
    
}
