//
//  HTTPBodyEncoder.swift
//  Test
//
//  Created by Filip Laššo on 28/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import Foundation

struct HTTPBodyEncoder {
    
    func encode(
        urlRequest: inout URLRequest,
        with bodyParameters: [String : Any]
    ) {
        guard let contentType = urlRequest.allHTTPHeaderFields?["Content-Type"] else { return }
        
        var data: Data?
        if contentType.contains("application/json") {
            data = try? JSONSerialization.data(withJSONObject: bodyParameters, options: .prettyPrinted)
        } else if contentType.contains("application/x-www-form-urlencoded") {
            data = bodyParameters.map{"\($0)=\($1)"}.joined(separator: "&").data(using: .utf8)
        }
        
        urlRequest.httpBody = data
    }
    
}
