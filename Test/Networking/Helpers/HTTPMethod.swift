//
//  HTTPMethod.swift
//  Test
//
//  Created by Filip Laššo on 22/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

public enum HTTPMethod: String {
    
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
    
}
