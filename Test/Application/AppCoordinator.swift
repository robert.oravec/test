//
//  AppCoordinator.swift
//  Test
//
//  Created by Filip Laššo on 14/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import UIKit

class RootCoordinator: Coordinator {
    let window: UIWindow
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(window: UIWindow, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = navigationController
    }
    
    func start() {
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}
