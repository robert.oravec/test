//
//  Coordinator+Extension.swift
//  Test
//
//  Created by Filip Laššo on 13/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import Foundation
import UIKit

extension Coordinator {
    
    // MARK: - Properties
    
    var presentedController: UIViewController? { UIApplication.shared.presentedControllerInKeyWindow }
    
}
