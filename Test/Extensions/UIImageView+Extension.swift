//
//  UIImageView+Extension.swift
//  Test
//
//  Created by Filip Laššo on 03/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import UIKit

extension UIImageView {
    
    private static var taskKey = 0
    private static var urlKey = 0

    private var currentTask: URLSessionTask? {
        get { return objc_getAssociatedObject(self, &UIImageView.taskKey) as? URLSessionTask }
        set { objc_setAssociatedObject(self, &UIImageView.taskKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    private var currentURL: URL? {
        get { return objc_getAssociatedObject(self, &UIImageView.urlKey) as? URL }
        set { objc_setAssociatedObject(self, &UIImageView.urlKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    func loadImageAsync(with urlString: String?) {
        // cancel prior task, if any
        weak var oldTask = currentTask
        currentTask = nil
        oldTask?.cancel()

        // reset imageview's image
        self.image = nil

        // allow supplying of nil to remove old image and then return immediately
        guard let urlString = urlString else { return }

        // check cache
        if let cachedImage = ImageCache.shared.image(forKey: urlString) {
            self.transition(toImage: cachedImage)
            return
        }

        // download
        let url = URL(string: urlString)!
        currentURL = url
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            self?.currentTask = nil

            // error handling
            if let unwrappedError = error {
                if (unwrappedError as NSError).domain == NSURLErrorDomain && (unwrappedError as NSError).code == NSURLErrorCancelled {
                    return
                }
                print(unwrappedError)
                return
            }

            guard let unwrappedData = data, let downloadedImage = UIImage(data: unwrappedData) else {
                print("unable to extract image")
                return
            }

            ImageCache.shared.save(image: downloadedImage, forKey: urlString)

            if url == self?.currentURL {
                DispatchQueue.main.async {
                    self?.transition(toImage: downloadedImage)
                }
            }
        }

        // save and start new task
        currentTask = task
        task.resume()
    }

    private func transition(toImage image: UIImage) {
        UIView.transition(with: self, duration: 0.3, options: [.transitionCrossDissolve], animations: {
            self.image = image
        }, completion: nil)
    }
    
}
