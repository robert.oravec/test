//
//  KeychainSwift+Extension.swift
//  Test
//
//  Created by Filip Laššo on 15/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import Foundation
import KeychainSwift

extension KeychainSwift {
    
    enum Keys {
        static let password = "Password"
    }
    
}
