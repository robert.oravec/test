//
//  UIApplication+Extension.swift
//  Test
//
//  Created by Filip Laššo on 13/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
    /// Get key window of application which according iOS version.
    /// - returns: Key window of application.
    func getKeyWindow() -> UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first(where: { $0.isKeyWindow })
        } else {
            return UIApplication.shared.keyWindow
        }
    }
    
    /// Get presented controller of key window
    var presentedControllerInKeyWindow: UIViewController? {
        var presentedController = getKeyWindow()?.rootViewController
        while let topController = presentedController?.presentedViewController {
            presentedController = topController
        }
        return presentedController
    }
    
}
