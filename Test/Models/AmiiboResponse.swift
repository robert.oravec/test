//
//  AmiiboResponse.swift
//  Test
//
//  Created by Filip Laššo on 28/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

import Foundation

struct AmiiboResponse: Codable {
    
    let amiibo: [Amiibo]
    
}
