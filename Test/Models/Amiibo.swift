//
//  Course.swift
//  Test
//
//  Created by Filip Laššo on 28/12/2020.
//  Copyright © 2020 Bluesoft s.r.o. All rights reserved.
//

struct Amiibo: Codable {
    
    let amiiboSeries: String
    let character: String
    let gameSeries: String
    let head: String
    let image: String
    let name: String
    let release: AmiiboRelease
    let tail: String
    let type: String
    
}
