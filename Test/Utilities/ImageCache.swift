//
//  ImageCache.swift
//  Test
//
//  Created by Filip Laššo on 03/01/2021.
//  Copyright © 2021 Bluesoft s.r.o. All rights reserved.
//

import Foundation
import UIKit

class ImageCache {
    
    private let cache = NSCache<NSString, UIImage>()

    static let shared = ImageCache()
    
    func image(forKey key: String) -> UIImage? {
        return cache.object(forKey: key as NSString)
    }

    func save(image: UIImage, forKey key: String) {
        cache.setObject(image, forKey: key as NSString)
    }
    
}
